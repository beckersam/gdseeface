use godot::prelude::{Vector2, Vector3, Quaternion};

pub const POINTS: usize = 68;

pub struct OSFFeatures {
    // This field indicates whether the left eye is opened(0) or closed (-1). A value of 1 means open wider than normal."/
    pub eye_left: f64,
    // This field indicates whether the right eye is opened(0) or closed (-1). A value of 1 means open wider than normal."/
    pub eye_right: f64,
    // This field indicates how steep the left eyebrow is, compared to the median steepness."/
    pub eyebrow_steepness_left: f64,
    // This field indicates how far up or down the left eyebrow is, compared to its median position."/
    pub eyebrow_up_down_left: f64,
    // This field indicates how quirked the left eyebrow is, compared to its median quirk."/
    pub eyebrow_quirk_left: f64,
    // This field indicates how steep the right eyebrow is, compared to the average steepness."/
    pub eyebrow_steepness_right: f64,
    // This field indicates how far up or down the right eyebrow is, compared to its median position."/
    pub eyebrow_up_down_right: f64,
    // This field indicates how quirked the right eyebrow is, compared to its median quirk."/
    pub eyebrow_quirk_right: f64,
    // This field indicates how far up or down the left mouth corner is, compared to its median position."/
    pub mouth_corner_up_down_left: f64,
    // This field indicates how far in or out the left mouth corner is, compared to its median position."/
    pub mouth_corner_in_out_left: f64,
    // This field indicates how far up or down the right mouth corner is, compared to its median position."/
    pub mouth_corner_up_down_right: f64,
    // This field indicates how far in or out the right mouth corner is, compared to its median position."/
    pub mouth_corner_in_out_right: f64,
    // This field indicates how open or closed the mouth is, compared to its median pose."/
    pub mouth_open: f64,
    // This field indicates how wide the mouth is, compared to its median pose."/
    pub mouth_wide: f64,
}

pub struct OSFData {
    // The time this tracking data was captured at."/
    pub time: f64,
    // This is the id of the tracked face. When tracking multiple faces, they might get reordered due to faces coming and going, but as long as tracking is not lost on a face, its id should stay the same. Face ids depend only on the order of first detection and locations of the faces."/
    pub id: i32,
    // This field gives the resolution of the camera or video being tracked."/
    pub camera_resolution: Vector2,
    // This field tells you how likely it is that the right eye is open."/
    pub right_eye_open: f64,
    // This field tells you how likely it is that the left eye is open."/
    pub left_eye_open: f64,
    // This field contains the rotation of the right eyeball."/
    pub right_gaze: Quaternion,
    // This field contains the rotation of the left eyeball."/
    pub left_gaze: Quaternion,
    // This field tells you if 3D points have been successfully estimated from the 2D points. If this is false, do not rely on pose or 3D data."/
    pub got_3d_points: bool,
    // This field contains the error for fitting the original 3D points. It shouldn't matter much, but it it is very high, something is probably wrong"/
    pub fit_3d_error: f64,
    // This is the rotation vector for the 3D points to turn into the estimated face pose."/
    pub rotation: Vector3,
    // This is the translation vector for the 3D points to turn into the estimated face pose."/
    pub translation: Vector3,
    // This is the raw rotation quaternion calculated from the OpenCV rotation matrix. It does not match Unity's coordinate system, but it still might be useful."/
    pub raw_quaternion: Quaternion,
    // This is the raw rotation euler angles calculated by OpenCV from the rotation matrix. It does not match Unity's coordinate system, but it still might be useful."/
    pub raw_euler: Vector3,
    // This field tells you how certain the tracker is."/
    pub confidence: [f64;POINTS],
    // These are the detected face landmarks in image coordinates. There are 68 points. The last too points are pupil points from the gaze tracker."/
    pub points: [Vector2;POINTS],
    // These are 3D points estimated from the 2D points. The should be rotation and translation compensated. There are 70 points with guesses for the eyeball center positions being added at the end of the 68 2D points."/
    pub points_3d: [Vector3;POINTS+2],
    // This field contains a number of action unit like features."/
    pub features: OSFFeatures,
}