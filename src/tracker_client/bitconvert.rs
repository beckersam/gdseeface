use super::tracker_data::{self, OSFFeatures};
use godot::{prelude::*, sys::__USE_ISOC11};

fn read_advance_f32(bytes: &[u8; 65535], pos: &mut usize) -> f32 {
let res = f32::from_be_bytes(bytes[(*pos)..(*pos+4)].try_into().expect("Invalid slice length"));
  *pos += 4;
  res
}

fn read_advance_quad(bytes: &[u8;65535], pos: &mut usize) -> Quaternion {
  Quaternion::new(
    read_advance_f32(bytes, pos),
    read_advance_f32(bytes, pos),
    read_advance_f32(bytes, pos),
    read_advance_f32(bytes, pos)
  )
}

fn read_advance_vec2(bytes: &[u8;65535], pos: &mut usize) -> Vector2 {
  Vector2::new(
    read_advance_f32(bytes, pos),
    read_advance_f32(bytes, pos)
  )
}

fn read_advance_vec3(bytes: &[u8;65535], pos: &mut usize) -> Vector3 {
  Vector3::new(
    read_advance_f32(bytes, pos),
    read_advance_f32(bytes, pos),
    read_advance_f32(bytes, pos)
  )
}

fn quad_look_rotation(forward: Vector3, up: Vector3) -> Quaternion {
  let forward = forward.normalized();
  let right = Vector3::cross(up, forward).normalized();
  let up = Vector3::cross(forward, right);

  let m00 = right.x;
  let m01 = right.y;
  let m02 = right.z;

  let m10 = up.x;
  let m11 = up.y;
  let m12 = up.z;

  let m20 = forward.x;
  let m21 = forward.y;
  let m22 = forward.z;

  let num8 = m00 + m11 + m22;

  if num8 > 0. {
    let num1 = (num8 + 1.0).sqrt();
    let num = 0.5 / num1;
    return Quaternion::new(
      (m12 - m21) * num,
      (m20 - m02) * num,
      (m01 - m10) * num,
      num1 * 0.5
    )
  }

  if m00 >= m11 && m00 >= m22 {
    let num7 = (((1.0 + m00) - m11) - m22).sqrt();
    let num4 = 0.5 / num7;
    return Quaternion::new(
      0.5 * num7,
      (m01 + m10) * num4,
      (m02 + m20) * num4,
      (m12 - m21) * num4
    )
  }

  if m11 > m22 {
    let num6 = (((1.0 + m11) - m00) - m22).sqrt();
    let num3 = 0.5 / num6;
    return Quaternion::new(
      (m10 + m01) * num3,
      0.5 * num6,
      (m21 + m12) * num3,
      (m20 - m02) * num3
    )
  }

  let num5 = (((1.0 + m22) - m00) - m11).sqrt();
  let num2 = 0.5 / num5;

  Quaternion::new(
    (m20 + m02) * num2,
    (m21 + m12) * num2,
    0.5 * num5,
    (m01 - m10) * num2
  )
}

fn swap_x(a: &Vector3) -> Vector3 {
  Vector3 { x: -a.x, y: a.y, z: a.z }
}

pub fn convert_bytes(bytes: [u8; 65535], total_count: usize) -> tracker_data::OSFData {
  let mut pos = 0;
  let time = f64::from_be_bytes(bytes[pos..pos+8].try_into().expect("Invalid slice length"));
  pos += 8;
  let id = i32::from_be_bytes(bytes[pos..pos+4].try_into().expect("Invalid slice length"));
  pos += 4;
  let camera_resolution = read_advance_vec2(&bytes, &mut pos);
  let right_eye_open = read_advance_f32(&bytes, &mut pos);
  let left_eye_open = read_advance_f32(&bytes, &mut pos);

  let got_3d_points = bytes[pos] != 0;
  pos += 1;

  let fit_3d_error = read_advance_f32(&bytes, &mut pos);
  let raw_quad = read_advance_quad(&bytes, &mut pos);
  let converted_quad = Quaternion::new(-raw_quad.x, raw_quad.y, -raw_quad.z, raw_quad.w);

  let raw_euler = read_advance_vec3(&bytes, &mut pos);
  let rotation = Vector3::new((raw_euler.x + 180.) % 360., raw_euler.y, (raw_euler.z - 90.) % 360.);

  let translation = Vector3::new(
    -read_advance_f32(&bytes, &mut pos), 
    read_advance_f32(&bytes, &mut pos), 
    -read_advance_f32(&bytes, &mut pos)
  );

  let mut confidence = [0.;tracker_data::POINTS];
  for i in 0..tracker_data::POINTS {
      confidence[i] = read_advance_f32(&bytes, &mut pos);
  };

  let mut points: [Vector2;tracker_data::POINTS] = [Vector2::ZERO; tracker_data::POINTS];
  for i in 0..tracker_data::POINTS {
      points[i] = read_advance_vec2(&bytes, &mut pos);
  };

  let mut points_3d: [Vector3;tracker_data::POINTS + 2] = [Vector3::ZERO; tracker_data::POINTS + 2];
  for i in 0..tracker_data::POINTS + 2 {
      points_3d[i] = read_advance_vec3(&bytes, &mut pos);
  };

  let right_gaze = quad_look_rotation(swap_x(&points_3d[66]) - swap_x(&points_3d[68]), Vector3::UP) * Quaternion::from_angle_axis(Vector3::RIGHT, 180.) * Quaternion::from_angle_axis(Vector3::FORWARD, 180.);
  let left_gaze = quad_look_rotation(swap_x(&points_3d[67]) - swap_x(&points_3d[69]), Vector3::UP) * Quaternion::from_angle_axis(Vector3::RIGHT, 180.) * Quaternion::from_angle_axis(Vector3::FORWARD, 180.);


  tracker_data::OSFData { time,
    id,
    camera_resolution,
    right_eye_open: right_eye_open as f64,
    left_eye_open: left_eye_open as f64,
    right_gaze,
    left_gaze,
    got_3d_points,
    fit_3d_error: fit_3d_error as f64,
    rotation,
    translation,
    raw_quaternion: raw_quad,
    raw_euler,
    confidence: confidence.map( |x| x as f64 ),
    points,
    points_3d,
    features: OSFFeatures {
      eye_left: read_advance_f32(&bytes, &mut pos) as f64,
      eye_right: read_advance_f32(&bytes, &mut pos) as f64,
      eyebrow_steepness_left: read_advance_f32(&bytes, &mut pos) as f64,
      eyebrow_up_down_left: read_advance_f32(&bytes, &mut pos) as f64,
      eyebrow_quirk_left: read_advance_f32(&bytes, &mut pos) as f64,
      eyebrow_steepness_right: read_advance_f32(&bytes, &mut pos) as f64,
      eyebrow_up_down_right: read_advance_f32(&bytes, &mut pos) as f64,
      eyebrow_quirk_right: read_advance_f32(&bytes, &mut pos) as f64,
      mouth_corner_up_down_left: read_advance_f32(&bytes, &mut pos) as f64,
      mouth_corner_in_out_left: read_advance_f32(&bytes, &mut pos) as f64,
      mouth_corner_up_down_right: read_advance_f32(&bytes, &mut pos) as f64,
      mouth_corner_in_out_right: read_advance_f32(&bytes, &mut pos) as f64,
      mouth_open: read_advance_f32(&bytes, &mut pos) as f64,
      mouth_wide: read_advance_f32(&bytes, &mut pos) as f64
    }
  }

}