use std::net::UdpSocket;
use std::thread;
use std::sync::mpsc;
use std::sync::mpsc::{Sender, Receiver};

pub mod tracker_data;
mod bitconvert;

pub struct OSFClient {
    pub received: i32,
    pub listening: bool,
    pub tracking_data: Vec<tracker_data::OSFData>,
    pub data_receiver: Option<Receiver<tracker_data::OSFData>>,

    ip: String,
    port: u32,
    thread: Option<thread::JoinHandle<()>>,
    client_shutdown_sender: Option<Sender<bool>>,
}

struct OSFReceiver {
    socket: UdpSocket,
    ip: String,
    port: u32,
    buffer: [u8; 65535],
    receiver: Receiver<bool>,
    sender: Sender<tracker_data::OSFData>,
}

impl OSFClient {
    pub fn new(ip: String, port: u32) -> OSFClient {
        OSFClient {
            ip,
            port,
            received: 0,
            listening: false,
            tracking_data: vec![],
            thread: None,
            data_receiver: None,
            client_shutdown_sender: None,
        }
    }

    pub fn start_receiving(&mut self) -> Result<(), std::io::Error>{
        let (shutdown_sender, shutdown_receiver): (Sender<bool>, Receiver<bool>) = mpsc::channel();
        self.client_shutdown_sender = Some(shutdown_sender);

        let (data_sender, data_receiver): (Sender<tracker_data::OSFData>, Receiver<tracker_data::OSFData>) = mpsc::channel();
        self.data_receiver = Some(data_receiver);

        let mut osf_receiver = OSFReceiver::new(self.ip.as_str().clone().to_string(), self.port, shutdown_receiver, data_sender);

        match osf_receiver.socket.connect(self.ip.as_str().to_owned() + self.port.to_string().as_str()) {
            Ok(_) => {}
            Err(err) => { return Err(err); }
        };

        let _thread = thread::spawn(move || {
            match osf_receiver.receiver.try_recv() {
                Ok(val) => {
                    if val {
                        return;
                    }
                }
                Err(val) => {godot::log::godot_warn!("OSF shutdown notifier error: {val}")}
            }

            match osf_receiver.socket.recv(&mut osf_receiver.buffer) {
                Ok(bytes_read) => {
                    osf_receiver.sender.send(bitconvert::convert_bytes(osf_receiver.buffer, bytes_read)).unwrap();
                }
                Err(err) => {godot::log::godot_warn!("OSF data receive error: {err}")}
            }
        });

        Ok(())
    }
}

impl OSFReceiver {
    fn new(ip: String, port: u32, receiver: Receiver<bool>, sender: Sender<tracker_data::OSFData>) -> OSFReceiver {
        
        OSFReceiver { 
            socket: UdpSocket::bind("127.0.0.1:11573").unwrap(),
            ip,
            port,
            buffer: [0;65535],
            sender,
            receiver,
        }
    }

}