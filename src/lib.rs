use godot::engine::Engine;
use godot::prelude::*;
use godot::engine::Sprite2D;
use godot::engine::Sprite2DVirtual;

mod tracker_server;
mod tracker_client;

struct MyExtension;

#[gdextension]
unsafe impl ExtensionLibrary for MyExtension {}

#[derive(GodotClass)]
#[class(base=Sprite2D)]
struct Player {
    speed: f64,
    angular_speed: f64,

    #[base]
    sprite: Base<Sprite2D>
}

#[godot_api]
impl Sprite2DVirtual for Player {
    fn init(sprite: Base<Sprite2D>) -> Self {
        println!("Hello from Rust!");

        Self { speed: 400.0, angular_speed: std::f64::consts::PI, sprite }
    }

    fn physics_process(&mut self, delta: f64) {
        if Engine::singleton().is_editor_hint() {
            return;
        }
        // Would be rotation += angular_speed * delta
        self.sprite.rotate((self.angular_speed * delta) as f32);
        let velocity = Vector2::UP.rotated(self.sprite.get_rotation()) * self.speed as f32;
        self.sprite.translate(velocity * delta as f32);
    }
}

#[godot_api]
impl Player {
    #[func]
    fn increase(&mut self, amount: f64) {
        self.speed += amount;
        self.emit_signal("speed_increased".into(), &[]);
    }

    #[signal]
    fn speed_increased();
}