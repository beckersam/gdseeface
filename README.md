# What
GDSeeFace intends to be a Extension for the Godot game engine (v4.1.1+) using the GDExtension system.

It aims to provide face tracking capabilities using the OpenSeeFace project.

## Structure
- /: Rust library project
- /Gdseeface: Example Godot project
- /OpenSeeFace: Locked version of OpenSeeFace